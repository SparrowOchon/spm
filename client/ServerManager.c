#include "ServerManager.h"
#include "Helper.h"
#include <curl/curl.h>

/**
 * @brief Path to Server serving the Packages.
 */
static const char *DEFAULT_URL = "localhost:3000/";
//-------------------------------Declaration-----------------------------------
static void init_web_response(web_response *json_string);
static size_t callback_func(void *ptr, size_t size, size_t nmemb, web_response *json_string);
static char *get_url(const char *package_name);
static void validate_json(struct json_object **json_struct);
static void json_to_screen(struct json_object **parsed_json);
//------------------------------------------------------------------------------

/**
 * @brief	Initialize the web response struct
 * @param json	Initialize the Json struct to the HEAP to later store Curl response
 */
static void init_web_response(web_response *json_string)
{
	json_string->len = 0;
	json_string->ptr = malloc(1);
	if (json_string->ptr == NULL) {
		fprintf(stderr, "malloc() failed\n");
		exit(EXIT_FAILURE);
	}
	json_string->ptr[0] = '\0';
}

/**
 * @brief Callback function for to write Web response to Web_response object
 * @param ptr	Pointer to data received from curl request.
 * @param size	Size of datatype we are working with
 * @param nmemb	Number of memebers of datatype we are working with
 * @param json_string Struct to store Web Response in
 * @return	The amount of data stored.
 */
static size_t callback_func(void *ptr, size_t size, size_t nmemb, web_response *json_string)
{
	size_t new_len = json_string->len + size * nmemb;
	json_string->ptr = realloc(json_string->ptr, new_len + 1);
	if (json_string->ptr == NULL) {
		fprintf(stderr, "realloc() failed\n");
		exit(EXIT_FAILURE);
	}
	memcpy(json_string->ptr + json_string->len, ptr, size * nmemb);
	json_string->ptr[new_len] = '\0';
	json_string->len = new_len;

	return size * nmemb;
}

/**
 * @brief	Get url to query for package provided
 * @param package_name	Package whose Information we need from the server
 * @return Return the URL (Including parameters) to request
 */
static char *get_url(const char *package_name)
{
	if (package_name == NULL) {
		return (char *)DEFAULT_URL;
	} else {
		return str_concat(2, DEFAULT_URL, package_name, NULL);
	}
}

/**
 * @brief	Validate if the JSON received from the server is Correct.
 * @param json_struct	Struct to validate.
 */
static void validate_json(struct json_object **json_struct)
{
	struct json_object *json_array_index = *json_struct;

	if (json_object_get_type(*json_struct) == json_type_array) {
		json_array_index = json_object_array_get_idx(*json_struct, 0);
	}
	if (json_object_get_string(json_object_object_get(json_array_index, "Name")) == NULL) {
		printf("Package = %s\n", json_object_to_json_string(json_array_index));
		json_object_put(*json_struct);
		exit(EXIT_FAILURE);
	}
}

/**
 * @brief Displaying the JSON on the screen (Used by print_json)
 * @param parsed_json	JSON struct which we want to write on screen.
 */
static inline void json_to_screen(struct json_object **parsed_json)
{
	printf("Name: %s\tSource: %s\tExtension: %s\tVersion: %s\tInitial_data: %s\n",
	       json_object_get_string(json_object_object_get(*parsed_json, "Name")),
	       json_object_get_string(json_object_object_get(*parsed_json, "Source")),
	       json_object_get_string(json_object_object_get(*parsed_json, "Extension")),
	       json_object_get_string(json_object_object_get(*parsed_json, "Version")),
	       json_object_get_string(json_object_object_get(*parsed_json, "Initial_date")));
}

/**
 * @brief	Query the server for a specified packaged name
 * @param package_name	Name to query the server for
 * @return	JSON struct containing the fetched data from the server.
 */
struct json_object *get_package_info(const char *package_name)
{
	struct json_object *parsed_json = NULL;
	CURL *curl = curl_easy_init();

	if (curl) {
		web_response json;

		init_web_response(&json);
		char *server_url = get_url(package_name);
		curl_easy_setopt(curl, CURLOPT_URL, server_url);
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, callback_func);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &json);
		CURLcode res = curl_easy_perform(curl);
		parsed_json = json_tokener_parse(json.ptr);
		free(json.ptr);
		json.ptr = NULL;
		if (server_url != DEFAULT_URL) {
			free(server_url);
			server_url = NULL;
		}

		if (res != CURLE_OK) {
			fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
		}
		curl_easy_cleanup(curl);

		validate_json(&parsed_json);
	}
	return parsed_json;
}

/**
 * @brief	Print json to screen.
 * @param parsed_json	Json object which we want to print to screen
 */
void print_json(struct json_object **parsed_json)
{
	struct json_object *json_array_index = NULL;
	if (json_object_get_type(*parsed_json) == json_type_array) {
		for (size_t i = 0; i < json_object_array_length(*parsed_json); ++i) {
			json_array_index = json_object_array_get_idx(*parsed_json, i);
			json_to_screen(&json_array_index);
		}
	} else {
		json_to_screen(parsed_json);
	}
}
