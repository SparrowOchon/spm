#ifndef DBMANAGER_H_
#define DBMANAGER_H_
#include <sqlite3.h>
#include <stdio.h>

/**
 * @brief Fields present in each row of the DB.
 */
typedef struct db_columns {
	char *name;
	char *source;
	char *extension;
	char *version;
	char *installed_date;
	char *installed_path;
} db_row;

/**
 * @brief  Get the Row count received from the db_select statement
 * @return Row count.
 */
size_t get_struct_size();

/**
 * @brief Fetch a list rows from the DB
 * @param package_name a String of primary key which we want to search again
 * @param select_type The type of fetch to preform ; 0 = Based on Package_Name, 1 = All packages in the DB.
 * @return Heap allocated Array of DB_ROW structs containing the rows received from the DB.
 */
db_row *db_select(const char *package_name, int select_type);

/**
 * @brief Updated the values of db row based on the Name.
 * @param db_entry Parameters to be altered in the DB (Name is a primary key i.e no duplicate)
 */
void db_update(const db_row *db_entry);

/**
 * @brief Insert the values of a db_row struct into the DB
 * @param db_entry Parameters to be inserted in the DB
 */
void db_insert(const db_row *db_entry);

/**
 * @brief Remove a DB row based on its package_name
 * @param package_name Primary key of the row which we want to remove.
 */
void db_remove(const char *package_name);

#endif /* DBMANAGER_H_ */
