#include "InputManager.h"
#include <stdlib.h>
/**
 * @brief List of Error Messages
 */
static const char argp_errors[][50] = {
	/* 00 */ "OK.",
	/* 01 */ "Too few arguments provided",
	/* 02 */ "Too many argument provided",
	/* 03 */ "Unable to parse command line arguments."
};

/**
 * @brief  Available/Supported Command line Options
 */
static const struct argp_option options[6] = { { "install", 'i', "PACKAGE_NAME", 0, "Install a package" },
					       { "uninstall", 'r', "PACKAGE_NAME", 0, "Remove an installed package" },
					       { "update", 'u', "PACKAGE_NAME", OPTION_ARG_OPTIONAL,
						 "Update an existing package" },
					       { "list-installed", 'l', 0, 0, "List all installed packages" },
					       { "list-available", 'a', 0, 0, "List all downloadable packages" },
					       { 0 } };

//-------------------------------Declaration-----------------------------------
static void print_input_error(struct argp_state *state, int err_num);
static error_t parse_user_arguments(int key, char *arg, struct argp_state *state);
//-----------------------------------------------------------------------------

/**
 * @brief Error Handler, Print appropriate error from argp_errors
 * @param state : State of argp.
 * @param err_num : Array index of appropriate error code in argp_error
 */
static void print_input_error(struct argp_state *state, int err_num)
{
	argp_failure(state, 1, 0, argp_errors[err_num]);
	exit(EXIT_FAILURE);
}

/**
 * @brief Input parser. Fetch user input and validate it.
 * @param key user inputed key (In ascii)
 * @param arg user inputed argument
 * @param state State of argp_state
 * @return Error Code, 0 if none occurred
 */
static error_t parse_user_arguments(int key, char *arg, struct argp_state *state)
{
	arguments *arguments = state->input;
	switch (key) {
	case 'i':
	case 'r':
		if (arg == NULL) {
			print_input_error(state, 1);
		}
	case 'u':
		if (arg != NULL) {
			arguments->args = arg;
		}
	case 'l': // Arg_option Arguments arent interpreted here onward.
	case 'a':
		arguments->key = key;
		break;
	default:
		break;
	}
	return 0;
}

/**
 * @brief Public callable argp object containing valid options and Input handler
 */
const struct argp argp = { options, parse_user_arguments };
