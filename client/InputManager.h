#ifndef INPUTMANAGER_H_
#define INPUTMANAGER_H_
#include <argp.h>

/**
 * @brief Store users passed in argument and key pressed
 */
typedef struct user_arguments {
	char *args;
	int key;
} arguments;

/**
 * @brief Public callable argp object containing valid options and Input handler
 */
const struct argp argp;

#endif /* INPUTMANAGER_H_ */
