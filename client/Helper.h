#ifndef HELPER_H
#define HELPER_H

/**
 * @brief	Get path relative to EXEC to the requested path
 * @param path	Directory which we want to find relative to our CWD
 * @return	The full path to the requested path
 */
char *get_path_to(const char *path);

/**
 * @brief	String concatenate onto the HEAP
 * @param str_count	The amount of string we want to concat
 * @param string1	The string who we will be storing first
 * @param string2	First string which we will concatenate
 * @param string3	The Second string which we will concatenate
 * @return	A Heap allocated string which is a concatenation of the passed parameters.
 */
char *str_concat(const int str_count, const char *string1, const char *string2, const char *string3);

#endif //HELPER_H
