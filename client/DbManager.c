#include "DbManager.h"
#include "Helper.h"
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

/**
 * @brief Count of Rows present in the Returned DB data on Select.
 */
static size_t ROW_INDEX = 0;
/**
 * @brief Connection object to store Active DB Connection
 */
static sqlite3 *DB_CONN = NULL;

//-------------------------------Declaration-----------------------------------
static void connect_db();
static void disconnect_db();
static void prepare_sql(const char *sql, sqlite3_stmt **sql_stmt);
static void exec_sql(const db_row *db_entry, sqlite3_stmt **sql_stmt);
static size_t db_count();
//------------------------------------------------------------------------------

/**
 * @brief Initialize a db connection to DB_FILE.
 */
static void connect_db()
{
	int rc = SQLITE_FAIL;
	if (DB_CONN == NULL) {
		const char *db_file = "/spm_client.db";
		char *db_path = get_path_to(db_file);
		if (db_path != NULL) {
			rc = sqlite3_open(db_path, &DB_CONN);
		}
		free(db_path);
		db_path = NULL;
		if (rc != SQLITE_OK) {
			fprintf(stderr, "Cannot open database: %s\n", sqlite3_errmsg(DB_CONN));
			exit(EXIT_FAILURE);
		}
	}
}

/**
 * @brief Close DB connection.
 */
static void disconnect_db()
{
	if (DB_CONN != NULL) {
		sqlite3_close(DB_CONN);
		DB_CONN = NULL;
	}
}

/**
 * @brief Bind an sql statement
 * @param sql SQL statement which we want to prepare
 * @param sql_stmt Sqlite3 struct to store the DB statement instance
 */
static void prepare_sql(const char *sql, sqlite3_stmt **sql_stmt)
{
	connect_db();
	if (sqlite3_prepare_v2(DB_CONN, sql, -1, sql_stmt, NULL)) {
		fprintf(stderr, "Failed to fetch data: %s\n", sqlite3_errmsg(DB_CONN));
		disconnect_db();
		exit(EXIT_FAILURE);
	}
}

/**
 * @brief Exec pre-prepared sql statement
 * @param db_entry (Array) Parameters to bind to the sql statement
 * @param sql_stmt Pre-prepared DB statement instance to be binded and executed against.
 */
static void exec_sql(const db_row *db_entry, sqlite3_stmt **sql_stmt)
{
	sqlite3_bind_text(*sql_stmt, 1, db_entry->name, -1, SQLITE_STATIC);
	sqlite3_bind_text(*sql_stmt, 2, db_entry->source, -1, SQLITE_STATIC);
	sqlite3_bind_text(*sql_stmt, 3, db_entry->extension, -1, SQLITE_STATIC);
	sqlite3_bind_text(*sql_stmt, 4, db_entry->version, -1, SQLITE_STATIC);
	sqlite3_bind_text(*sql_stmt, 5, db_entry->installed_date, -1, SQLITE_STATIC);
	sqlite3_bind_text(*sql_stmt, 6, db_entry->installed_path, -1, SQLITE_STATIC);
	while (sqlite3_step(*sql_stmt) == SQLITE_ROW) {
		printf("%s\n", sqlite3_column_text(*sql_stmt, 0));
	}
	sqlite3_finalize(*sql_stmt);
	disconnect_db();
}

/**
 * @brief	Return the Local DB row count.
 * @return The total Row count of the Local DB
 */
static size_t db_count()
{
	sqlite3_stmt *sql_stmt = NULL;
	size_t column_count = 0;
	static const char *size_sql = "Select count(name) from spm_packages;";

	prepare_sql(size_sql, &sql_stmt);
	if (sqlite3_step(sql_stmt) == SQLITE_ROW) {
		column_count = (size_t)sqlite3_column_int64(sql_stmt, 0);
	}
	sqlite3_finalize(sql_stmt);
	return column_count;
}

/**
 * @brief Fetch a list rows from the DB
 * @param package_name a String of primary key which we want to search again
 * @param select_type The type of fetch to preform ; 0 = Based on Package_Name, 1 = All packages in the DB.
 * @return Heap allocated Array of DB_ROW structs containing the rows received from the DB.
 */
db_row *db_select(const char *package_name, const int select_type)
{
	sqlite3_stmt *sql_stmt = NULL;
	db_row *sql_rows = NULL;
	size_t column_count = 1;

	if (select_type == 0) {
		// Select a specific Package
		static const char *select_sql = "Select name,source,extension,version,installed_date,"
						"installed_path from spm_packages where name=?";
		prepare_sql(select_sql, &sql_stmt);
		sqlite3_bind_text(sql_stmt, 1, package_name, -1, SQLITE_STATIC);
	} else {
		// Select all Packages
		static const char *all_sql = "Select name,source,extension,version,installed_date,"
					     "installed_path from spm_packages";
		prepare_sql(all_sql, &sql_stmt);
		column_count = db_count();
	}
	sql_rows = malloc((sizeof(db_row) * column_count) + sizeof(char *));
	sql_rows[0].name = NULL; // Initialize Name to Null as needed to validate package presence.

	while (sqlite3_step(sql_stmt) == SQLITE_ROW) {
		sql_rows[ROW_INDEX].name = strndup((char *)sqlite3_column_text(sql_stmt, 0),
						   strlen((char *)sqlite3_column_text(sql_stmt, 0)));
		sql_rows[ROW_INDEX].source = strndup((char *)sqlite3_column_text(sql_stmt, 1),
						     strlen((char *)sqlite3_column_text(sql_stmt, 1)));
		sql_rows[ROW_INDEX].extension = strndup((char *)sqlite3_column_text(sql_stmt, 2),
							strlen((char *)sqlite3_column_text(sql_stmt, 2)));
		sql_rows[ROW_INDEX].version = strndup((char *)sqlite3_column_text(sql_stmt, 3),
						      strlen((char *)sqlite3_column_text(sql_stmt, 3)));
		sql_rows[ROW_INDEX].installed_date = strndup((char *)sqlite3_column_text(sql_stmt, 4),
							     strlen((char *)sqlite3_column_text(sql_stmt, 4)));
		sql_rows[ROW_INDEX].installed_path = strndup((char *)sqlite3_column_text(sql_stmt, 5),
							     strlen((char *)sqlite3_column_text(sql_stmt, 5)));
		++ROW_INDEX;
	}
	sqlite3_finalize(sql_stmt);
	disconnect_db();
	return sql_rows;
}

/**
 * @brief  Get the Row count received from the db_select statement
 * @return Row count.
 */
size_t get_struct_size()
{
	return ROW_INDEX;
}

/**
 * @brief Updated the values of db row based on the Name.
 * @param db_entry Parameters to be altered in the DB (Name is a primary key i.e no duplicate)
 */
void db_update(const db_row *db_entry)
{
	sqlite3_stmt *sql_stmt = NULL;
	static const char *update_sql = "Update spm_packages set name=?,source=?,extension=?,"
					"version=?,installed_date=?,installed_path=? WHERE name=?";
	prepare_sql(update_sql, &sql_stmt);
	sqlite3_bind_text(sql_stmt, 7, db_entry->name, -1, SQLITE_STATIC);
	exec_sql(db_entry, &sql_stmt);
}

/**
 * @brief Insert the values of a db_row struct into the DB
 * @param db_entry Parameters to be inserted in the DB
 */
void db_insert(const db_row *db_entry)
{
	sqlite3_stmt *sql_stmt = NULL;
	static const char *insert_sql = "Insert into spm_packages (name,source,extension,version,"
					"installed_date,installed_path) VALUES (?,?,?,?,?,?)";
	prepare_sql(insert_sql, &sql_stmt);
	exec_sql(db_entry, &sql_stmt);
}

/**
 * @brief Remove a DB row based on its package_name
 * @param package_name Primary key of the row which we want to remove.
 */
void db_remove(const char *package_name)
{
	sqlite3_stmt *sql_stmt = NULL;
	static const char *delete_sql = "Delete from spm_packages where name=?";

	prepare_sql(delete_sql, &sql_stmt);
	sqlite3_bind_text(sql_stmt, 1, package_name, -1, 0);
	if (sqlite3_step(sql_stmt) == SQLITE_ROW) {
		printf("%s\n", sqlite3_column_text(sql_stmt, 0));
	}
	sqlite3_finalize(sql_stmt);
	disconnect_db();
}
