#define _XOPEN_SOURCE 500
#include <ftw.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <curl/curl.h>
#include <stdio.h>
#include <string.h>

#include "FileManager.h"
#include "Helper.h"

static const char *DOWNLOAD_DIR = "/Downloads/";
static inline int check_if_path_exist(const char *local_path);
static int create_folder(const char *dir, const char *local_dir);
static int extract_file(const char *extract_dir, const char *archived_file, const char *package_name);
static int remove_dir(const char *fpath, const struct stat *sb, int tflag, struct FTW *ftwbuf);
static int copy_data(struct archive *ar, struct archive *aw);

/**
 * @brief Callback function to write curl response to a file
 * @param ptr Pointer to data received from curl request.
 * @param size Size of datatype we are working with
 * @param nmemb	Number of memebers of datatype we are working with
 * @param stream Stream to store Curl data into a file.
 * @return The amount of data stored.
 */
static inline size_t write_data(void *ptr, size_t size, size_t nmemb, void *stream)
{
	return fwrite(ptr, size, nmemb, (FILE *)stream);
}

/**
 * @brief Check if a Directory or File exists. I.e if the path is valid
 * @param local_path Path which we want to validate as a String
 * @return 0 or -1 if an error occurred.
 */
static inline int check_if_path_exist(const char *local_path)
{
	struct stat stat_struct = {};
	return (stat(local_path, &stat_struct) != -1);
}

/**
 * @brief Create a directory at a pre-defined existing path
 * @param dir The existing path under which we want to create a dir
 * @param local_dir The directory which we want to create
 * @return 0 or EXIT_FAILURE if an error occurred.
 */
static int create_folder(const char *dir, const char *local_dir)
{
	int result = 0;
	char *dir_path = NULL;

	mkdir(dir, 0755);
	dir_path = str_concat(2, dir, local_dir, NULL);
	result = mkdir(dir_path, 0755);
	free(dir_path);
	dir_path = NULL;
	switch (result) {
	case EACCES:
		fprintf(stderr, "Permission denied in to create: %s\n", local_dir);
		return (EXIT_FAILURE);
	case EEXIST:
		fprintf(stderr, "Filename already exists: %s\n", local_dir);
		return (EXIT_FAILURE);
	case EMLINK:
		fprintf(stderr, "Parent directory has too many links to create: %s\n", local_dir);
		return (EXIT_FAILURE);
	case ENOSPC:
		fprintf(stderr, "Not enough Diskspace: %s\n", local_dir);
		return (EXIT_FAILURE);
	case EROFS:
		fprintf(stderr, "File creation attempt on a Read-only filesystem: %s\n", local_dir);
		return (EXIT_FAILURE);
	case EXIT_FAILURE:
		fprintf(stderr, "Failed, Folder not present: %s\n", local_dir);
		return (EXIT_FAILURE);

	default:
		printf("Folder created: %s\n", local_dir);
	}
	return 0;
}

/**
 * @brief   Copy data from one Archive to another
 * @param ar   Archive whose data we want to read from
 * @param aw    Archive whose who we want to write to
 * @return   0 or ERROR CODE dependant on if an error occurred.
 */
static int copy_data(struct archive *ar, struct archive *aw)
{
	int r;
	const void *buff;
	size_t size;
	la_int64_t offset;

	for (;;) {
		r = archive_read_data_block(ar, &buff, &size, &offset);
		if (r == ARCHIVE_EOF)
			return (ARCHIVE_OK);
		if (r < ARCHIVE_OK)
			return (r);
		r = archive_write_data_block(aw, buff, size, offset);
		if (r < ARCHIVE_OK) {
			fprintf(stderr, "%s\n", archive_error_string(aw));
			return (r);
		}
	}
}

/**
 * @brief Extract an Archived file at to directory under a directory named after the package
 * @param extract_dir Directory which to extract to
 * @param archived_file Archive which we wish to extract
 * @param package_name The folder under extract_dir which will
 *                     contain the extracted files of the archive.
 *
 * @return 0 or ERROR CODE dependant on if an error occurred.
 */
static int extract_file(const char *extract_dir, const char *archived_file, const char *package_name)
{
	struct archive *a;
	struct archive *ext;
	struct archive_entry *entry;
	int flags;
	int r;
	char *path = NULL;

	char *package_path = str_concat(2, package_name, "/", NULL);

	/* Select which attributes we want to restore. */
	flags = ARCHIVE_EXTRACT_TIME;
	flags |= ARCHIVE_EXTRACT_PERM;
	flags |= ARCHIVE_EXTRACT_ACL;
	flags |= ARCHIVE_EXTRACT_FFLAGS;

	a = archive_read_new();
	archive_read_support_format_all(a);
	archive_read_support_filter_all(a);
	ext = archive_write_disk_new();
	archive_write_disk_set_options(ext, flags);
	archive_write_disk_set_standard_lookup(ext);
	if ((r = archive_read_open_filename(a, archived_file, 10240))) {
		return (EXIT_FAILURE);
	}
	while (1) {
		r = archive_read_next_header(a, &entry);
		if (r == ARCHIVE_EOF) {
			break;
		}
		if (r < ARCHIVE_OK) {
			fprintf(stderr, "%s\n", archive_error_string(a));
		}
		if (r < ARCHIVE_WARN) {
			free(package_path);
			package_path = NULL;
			return (EXIT_FAILURE);
		}
		//archive_entry_set_pathname(entry, *filename);
		path = (char *)archive_entry_pathname(entry);
		path = str_concat(3, extract_dir, package_path, path);
		archive_entry_set_pathname(entry, path);
		r = archive_write_header(ext, entry);
		free(path);
		path = NULL;

		if (r < ARCHIVE_OK) {
			fprintf(stderr, "%s\n", archive_error_string(ext));
		} else if (archive_entry_size(entry) > 0) {
			r = copy_data(a, ext);
			if (r < ARCHIVE_OK) {
				fprintf(stderr, "%s\n", archive_error_string(ext));
			}
			if (r < ARCHIVE_WARN) {
				free(package_path);
				package_path = NULL;
				return (EXIT_FAILURE);
			}
		}
		r = archive_write_finish_entry(ext);
		if (r < ARCHIVE_OK) {
			fprintf(stderr, "%s\n", archive_error_string(ext));
		}
		if (r < ARCHIVE_WARN) {
			free(package_path);
			package_path = NULL;
			return (EXIT_FAILURE);
		}
	}
	archive_read_close(a);
	archive_read_free(a);
	archive_write_close(ext);
	archive_write_free(ext);
	free(package_path);
	package_path = NULL;

	return (EXIT_SUCCESS);
}

/**
 * @brief  File Tree Walk callback function to remove File and Directories.
 * @param fpath Path which we want to remove
 * @param sb
 * @param tflag File/Directory State flag. Determining what kind of file/directory the fpath is pointing to.
 * @param ftwbuf
 * @return  0 or ERROR CODE dependant on if an error occurred.
 */
static int remove_dir(const char *fpath, const struct stat *sb, int tflag, struct FTW *ftwbuf)
{
	switch (tflag) {
	case FTW_D:
	case FTW_DNR:
	case FTW_DP:
		rmdir(fpath);
		break;
	default:
		unlink(fpath);
		break;
	}
	return (0);
}

/**
 * @brief   Remove a File or Directory at the Path
 * @param local_path    Path at which we want to remove all files and directories.
 */
void remove_file(const char *local_path)
{
	if (check_if_path_exist(local_path)) {
		int remove_code = remove(local_path);
		switch (remove_code) {
		case 0:
			printf("File Removed: %s\n", local_path);
			break;
		case -1:
			nftw(local_path, remove_dir, 64, FTW_DEPTH | FTW_PHYS);
			printf("Folder Removed: %s\n", local_path);
			break;
		default:
			printf("Unable to delete the file: %s\n", local_path);
		}
	}
}

/**
 * @brief Download a package to a local directory.
 * @param package_url Source of package to download
 * @param package_name  Package name which we want to download
 * @param package_extension Extension of file which will be downloaded from package_url
 * @return Path to locally installed file.
 */
char *download_file(const char *package_url, const char *package_name, const char *package_extension)
{
	char *downloaded_file = NULL;
	CURLcode res;
	char *download_path = get_path_to(DOWNLOAD_DIR);

	if (download_path == NULL) {
		return NULL;
	}
	CURL *curl = curl_easy_init();

	if (curl) {
		int folder_status = create_folder(download_path, package_name);
		if (folder_status != 0) {
			free(download_path);
			download_path = NULL;
			return NULL;
		}

		downloaded_file = str_concat(3, download_path, package_name, package_extension);

		FILE *fp = fopen(downloaded_file, "wb");
		curl_easy_setopt(curl, CURLOPT_URL, package_url);
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
		res = curl_easy_perform(curl);
		/* always cleanup */
		curl_easy_cleanup(curl);
		fclose(fp);

		folder_status = extract_file(download_path, downloaded_file, package_name);
		remove_file(downloaded_file);
		free(downloaded_file);
		downloaded_file = NULL;
		if (res != CURLE_OK || folder_status != 0) {
			free(download_path);
			download_path = NULL;
			return NULL;
		}
		downloaded_file = str_concat(2, download_path, package_name, NULL);
	} else {
		printf("Unable to download file: %s\n", package_url);
	}
	free(download_path);
	download_path = NULL;
	return downloaded_file;
}

/**
 * @brief Update the Local Files at the provided Installed_Path by removing them and redownloading them.
 * @param installed_path    Path at which we want to purge of previously installed files.
 * @param package_name      Package name which we want to download again
 * @param package_url       Source of package to download
 * @param package_extension Extension of file which will be downloaded from package_url
 * @return Path to locally installed file.
 */
char *update_file(const char *installed_path, const char *package_name, const char *package_url,
		  const char *package_extension)
{
	remove_file(installed_path);
	return download_file(package_url, package_name, package_extension);
}
