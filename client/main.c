#include "DbManager.h"
#include "InputManager.h"
#include "ServerManager.h"
#include "FileManager.h"

#include <stdio.h>
#include <stdlib.h>

static void install_packages(const char *);
static void uninstall_packages(const char *);
static void update_packages(const char *);
static void list_installed();
static void list_available();
static void json_to_struct(db_row[], struct json_object **, size_t);
static void free_db_storage(db_row *db_struct);

int main(int argc, char **argv)
{
	arguments user_arguments;
	argp_parse(&argp, argc, argv, 0, 0, &user_arguments);

	switch (user_arguments.key) {
	case 'i':
		install_packages(user_arguments.args);
		break;
	case 'r':
		uninstall_packages(user_arguments.args);
		break;
	case 'u':
		update_packages(user_arguments.args);
		break;
	case 'l':
		list_installed();
		break;
	case 'a':
		list_available();
		break;
	default:
		break;
	}
	return 0;
}

/**
 * @brief Install a package
 * @param package_name Package name to install
 */
void install_packages(const char *package_name)
{
	printf("Installing Packages: %s\n", package_name);
	db_row *installed_package = db_select(package_name, 0);
	if (installed_package->name != NULL) {
		free_db_storage(installed_package);
		printf("Package already installed\n");
	} else {
		free_db_storage(installed_package);
		struct json_object *server_response_json = get_package_info(package_name);
		size_t json_package_count = 1;
		if (json_object_get_type(server_response_json) == json_type_array) {
			json_package_count = json_object_array_length(server_response_json);
		}
		db_row db_struct[json_package_count];
		json_to_struct(db_struct, &server_response_json, json_package_count);
		for (size_t i = 0; i < json_package_count; ++i) {
			if (db_struct[i].installed_path != NULL) {
				db_struct[i].installed_path =
					download_file(db_struct[i].source, db_struct[i].name, db_struct[i].extension);
				db_insert(&db_struct[i]);
				free(db_struct[i].installed_path);
				db_struct[i].installed_path = NULL;
			} else {
				printf("Unable to Install Package:%s\n", db_struct[i].name);
			}
		}
		json_object_put(server_response_json);
	}
}

/**
 * @brief Uninstall a package
 * @param package_name 	Package name to uninstall
 */
static void uninstall_packages(const char *package_name)
{
	printf("Removing Package: %s\n", package_name);
	db_row *installed_package = db_select(package_name, 0);
	if (installed_package->name != NULL) {
		remove_file(installed_package->installed_path);
		free_db_storage(installed_package);
		db_remove(package_name);
		printf("Package Uninstalled and Removed\n");
	} else {
		free_db_storage(installed_package);
		printf("Package Not Installed\n");
	}
}
/**
 * @brief Update the Package installation if a newer version is available in the server
 * @param package_name Package name to query the server for.
 */
static void update_packages(const char *package_name)
{
	db_row *installed_package = NULL;

	printf("Updating All Packages\n");
	if (package_name != NULL) {
		// Update Specific Package Name
		installed_package = db_select(package_name, 0);
		if (installed_package->name == NULL) {
			free_db_storage(installed_package);
			printf("Package Not Installed\n");
		} else {
			struct json_object *server_response_json = get_package_info(package_name);
			size_t json_package_count = 1;

			if (json_object_get_type(server_response_json) == json_type_array) {
				json_package_count = json_object_array_length(server_response_json);
			}
			db_row db_struct[json_package_count];
			json_to_struct(db_struct, &server_response_json, json_package_count);
			for (size_t i = 0; i < json_package_count; ++i) {
				if (strcmp(installed_package->version, db_struct[i].version) != 0) {
					printf("Updating %s\n", installed_package->name);
					db_struct[i].installed_path =
						update_file(installed_package->installed_path, db_struct[i].name,
							    db_struct[i].source, db_struct[i].extension);
					db_update(&db_struct[i]);
					free(db_struct[i].installed_path);
					db_struct[i].installed_path = NULL;
				} else {
					printf("Package %s is up to date\n", installed_package->name);
				}
			}
			json_object_put(server_response_json);
			free_db_storage(installed_package);
		}

	} else {
		// Update All packages
		installed_package = db_select("", 1);
		for (size_t i = 0; i < (get_struct_size()); ++i) {
			printf("Updating %s\n", installed_package->name);
			installed_package[i].installed_path =
				update_file(installed_package[i].installed_path, installed_package[i].name,
					    installed_package[i].source, installed_package[i].extension);
			db_update(&installed_package[i]);
		}
		free_db_storage(installed_package);
	}
}

/**
 * @brief List all installed packages. According to DB
 */
static void list_installed()
{
	printf("Listing Installed Packages\n");
	db_row *db_struct = db_select("", 1);
	for (size_t i = 0; i < (get_struct_size()); ++i) {
		printf("-----\nName:%s\nVersion:%s\nInstalled:%s\n-------\n\n", db_struct[i].name, db_struct[i].version,
		       db_struct[i].installed_path);
	}
	free_db_storage(db_struct);
}

/**
 * @brief List all available packages in the server.
 */
static void list_available()
{
	printf("Listing Available Packages\n");
	struct json_object *server_response_json = get_package_info(NULL);
	print_json(&server_response_json);
	json_object_put(server_response_json);
}

/**
 * @brief Transfor a json object to a db struct
 * @param db_struct     Array we want to store the JSON object in
 * @param json_struct   JSON object we wish to transform
 * @param json_length	Length of the JSON struct to store
 */
static void json_to_struct(db_row db_struct[], struct json_object **json_struct, const size_t json_length)
{
	struct json_object *json_array_index = *json_struct;

	for (size_t i = 0; i < json_length; ++i) {
		if (json_length != 0) { // Only 0 when array is zero
			json_array_index = json_object_array_get_idx(*json_struct, i);
		}
		db_struct[i].name = (char *)json_object_get_string(json_object_object_get(json_array_index, "Name"));
		db_struct[i].source =
			(char *)json_object_get_string(json_object_object_get(json_array_index, "Source"));
		db_struct[i].extension =
			(char *)json_object_get_string(json_object_object_get(json_array_index, "Extension"));
		db_struct[i].version =
			(char *)json_object_get_string(json_object_object_get(json_array_index, "Version"));
		db_struct[i].installed_date =
			(char *)json_object_get_string(json_object_object_get(json_array_index, "Initial_date"));
		db_struct[i].installed_path = "";
	}
}

/**
 * @brief Free Storage Reserved by Select Statement
 * @param db_struct DB Struct containing malloc'd strings
 */
static inline void free_db_storage(db_row *db_struct)
{
	if (get_struct_size() == 0) {
		free(db_struct[0].name);
		db_struct[0].name = NULL;
	} else {
		for (size_t i = 0; i < get_struct_size(); ++i) {
			free(db_struct[i].name);
			free(db_struct[i].extension);
			free(db_struct[i].source);
			free(db_struct[i].version);
			free(db_struct[i].installed_date);
			free(db_struct[i].installed_path);

			db_struct[i].name = NULL;
			db_struct[i].extension = NULL;
			db_struct[i].source = NULL;
			db_struct[i].version = NULL;
			db_struct[i].installed_date = NULL;
			db_struct[i].installed_path = NULL;
		}
	}
	free(db_struct);
	db_struct = NULL;
}
