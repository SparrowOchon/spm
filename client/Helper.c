#include "Helper.h"
#include <stdlib.h>
#include <memory.h>
#include <unistd.h>
#include <limits.h>

//-------------------------------Declaration-----------------------------------
static char *get_cwd();
//-----------------------------------------------------------------------------

/**
 * @brief Get current working directory relative to the EXEC path
 * @return String of current working directory.
 */
static char *get_cwd()
{
	char *cwd = malloc(PATH_MAX + 1);
	return getcwd(cwd, PATH_MAX);
}

/**
 * @brief	String concatenate onto the HEAP
 * @param str_count	The amount of string we want to concat
 * @param string1	The string who we will be storing first
 * @param string2	First string which we will concatenate
 * @param string3	The Second string which we will concatenate
 * @return	A Heap allocated string which is a concatenation of the passed parameters.
 */
char *get_path_to(const char *path)
{
	char *cwd = get_cwd();
	char *concat_path = NULL;
	if (cwd != NULL) {
		concat_path = str_concat(2, cwd, path, NULL);
	}
	free(cwd);
	return concat_path;
}

/**
 * @brief	Get path relative to EXEC to the requested path
 * @param path	Directory which we want to find relative to our CWD
 * @return	The full path to the requested path
 */
char *str_concat(const int str_count, const char *string1, const char *string2, const char *string3)
{
	size_t heap_size = 0;
	char *output_string = malloc(8);
	switch (str_count) {
	case 2:
		if (strlen(string1) != 0 && strlen(string2) != 0) {
			heap_size = (1 + strlen(string1) + strlen(string2));
			output_string = (char *)realloc(output_string, heap_size);

			strncpy(output_string, string1, heap_size);
			strncat(output_string, string2, heap_size);
		} else {
			output_string = NULL;
		}
		break;
	case 3:
		if (strlen(string1) != 0 && strlen(string2) != 0 && strlen(string2) != 0) {
			heap_size = (1 + strlen(string1) + strlen(string2) + strlen(string3));
			output_string = (char *)realloc(output_string, heap_size);
			strncpy(output_string, string1, heap_size);
			strncat(output_string, string2, heap_size);
			strncat(output_string, string3, heap_size);
		} else {
			output_string = NULL;
		}
		break;
	default:
		output_string = NULL;
	}
	return output_string;
}
