#ifndef FILEMANAGER_H
#define FILEMANAGER_H
#include <archive.h>
#include <archive_entry.h>

/**
 * @brief   Remove a File or Directory at the Path
 * @param local_path    Path at which we want to remove all files and directories.
 */
void remove_file(const char *local_path);

/**
 * @brief Update the Local Files at the provided Installed_Path by removing them and redownloading them.
 * @param installed_path    Path at which we want to purge of previously installed files.
 * @param package_name      Package name which we want to download again
 * @param package_url       Source of package to download
 * @param package_extension Extension of file which will be downloaded from package_url
 * @return Path to locally installed file.
 */
char *update_file(const char *installed_path, const char *package_name, const char *package_url,
		  const char *package_extension);

/**
 * @brief Download a package to a local directory.
 * @param package_url Source of package to download
 * @param package_name  Package name which we want to download
 * @param package_extension Extension of file which will be downloaded from package_url
 * @return Path to locally installed file.
 */
char *download_file(const char *package_url, const char *package_name, const char *package_extension);

#endif //FILEMANAGER_H
