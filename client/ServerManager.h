#ifndef SERVERMANAGER_H
#define SERVERMANAGER_H
#include <json-c/json.h>
#include <memory.h>

/**
 * @brief Struct to store the Response from the Website as a string
 */
typedef struct string {
	char *ptr;
	size_t len;
} web_response;

/**
 * @brief	Query the server for a specified packaged name
 * @param package_name	Name to query the server for
 * @return	JSON struct containing the fetched data from the server.
 */
struct json_object *get_package_info(const char *package_name);

/**
 * @brief	Print json to screen.
 * @param parsed_json	Json object which we want to print to screen
 */
void print_json(struct json_object **parsed_json);
#endif // SERVERMANAGER_H
