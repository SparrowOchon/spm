# System Package Manager

The following project was a challenge to create a full System Package Manager in 4 hours.

#### TIME TAKE: 4h 14 minutes

#### Features

##### Client

- Install Packages (Supports tar, pax, cpio, zip, xar, lha, ar, cab, mtree, rar, 7zip, CAB and ISO images. Compression with gzip, bzip2, lzip, xz and lzma )
- Uninstall Packages
- Update Installed Packages
- List all Available Packages
- List all Installed Packages

##### Server

- Find package info based on passed in Name and serve it as JSON
- Return all Packages as a JSON
- Support over 9000 Concurrent Clients

#### Why this Project

To self evaluate the following:

- Deadline Estimation
- Design Strength
- Efficiency with the Languages (C99, Golang 1.13)
- Efficiency under time constraints
  - Performant, Leak free Code
  - Relatively Optimized Heap Usage
  - Time to Code/Learn/Implement Libraries

#### Additional Information

1. Both databases have `auto_vacuum=incremental` already enabled to allow page freeing. Without slowing down Client or Server such as with `FULL`
2. C99 was the target platform for the Client, Golang 1.13 for the server.
3. C downloads folder will be created in the **SAME PATH AS THE BINARY**
4. Since it uses Golang 1.13 it can scale on cores automatically and using Goroutines server over 9000 clients at once.
5. No new data was added to the Repo post the 4h14 mark other than LICENSE and README update.

## The Architecture Diagram

![architecture](images/spm.jpg)

##### Diagram Facts:

- Package Name is unique.(Done through the DB PK)
- Each step has validation for its inputs.
- Each blue Rectangle _With rounded corners_ represents a Translation Unit.
- Each arrow signifies a transfer of information.
- Orange Rectangle Signifies Server Side.
- Giant Blue Rectangle signifies Client side.
- Both DBs are stored as files I.e not directly in Program Memory.

| Step |                                                     Action                                                     |
| :--: | :------------------------------------------------------------------------------------------------------------: |
|  1   |                                              Read in User input.                                               |
|  2   |                                 Filer input and Pass it to the ServerManager.                                  |
|  3   |                          Send a Get request to the Server for the Desired package(s).                          |
|  4   |                        Send a request to its local SQLManager to locate the package(s).                        |
|  5   |                           Server SQLManager queries the DB for the package name(s).                            |
|  6   |                            Return package information to Server to be served back.                             |
|  7   |                                      Return a JSON message to the Client.                                      |
|  8   |                          Parse through the JSON message and send it to the Dbmanager.                          |
|  9   | Parse through the Data and either insert/update it in the CLIENT side db. (STEP SKIPPED ON LIST ALL AVAILABLE) |
|  10  |                  Send the file download information to the FileManager to download the file.                   |
|  11  |                              Download the file locally on the Client's computer.                               |

## Installation

#### Client

The following dependencies were maintained using `Conan`. Thus it is a pre-req: `pip install conan`

```
mkdir build && cd build
conan install ..
cmake .. -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Release
cmake --build .
```

#### Server

```
go build server.go sqlserver.go
```

## Usage

#### Client

**NOTE** Argp-parse is not properly able to read in update parameters using `-u` thus `--update=<Package-Name>` needs to be used.

```
Usage: client [OPTION...]

  -a, --list-available       List all downloadable packages
  -i, --install=PACKAGE_NAME Install a package
  -l, --list-installed       List all installed packages
  -r, --uninstall=PACKAGE_NAME   Remove an installed package
  -u, --update[=PACKAGE_NAME]   Update an existing package
  -?, --help                 Give this help list
      --usage                Give a short usage message

Mandatory or optional arguments to long options are also mandatory or optional
for any corresponding short options.
```

#### Server

Once ran it will wait for connections on:

- `localhost:3000/<PackageName>` To fetch DB values of a single package.
- `locahost:3000/` To fetch all DB rows.

## Shortcuts taken to save time:

Due to the very small time window some shortcuts needed to be taken. All have been evaluated to not cause massive problems or can easily be fixed.

| Shortcut Taken                                                                                                                                                         | Solutions                                                                                                                                                                                                                                                      |
| ---------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| No checksum checking on server                                                                                                                                         | Add a checksum to the server and simply validate against it post file download.                                                                                                                                                                                |
| Server only able to retrieve one package at a time                                                                                                                     | Switch from attempting to read URL to a full variable based post request to browse based on packages.                                                                                                                                                          |
| Server will slow down when Garbage Collecting the HEAP (Mark and Sweap) while having over 10k Clients                                                                  | Fill some of the heap with a variable stored solely in the programs virtual memory. I.e referenced but never used. This will serve as a way to limit the Time till M&S without forcing HEAP size to increase with `GOGC`.                                      |
| Client fetches directly from the server.                                                                                                                               | Client should be pulling the DB and caching it to avoid hitting the server unless absolutely needed. Meaning when an update is being made it should first flush this cache and than update against the new stored Cache that has been fetched from the server. |
| Doesnt catch SIGNALS by the user and can cause leaks if killed mid process.                                                                                            | Add a SIGNAL handler to make sure the memory is properly freed. And cleanup is complete before Terminating.                                                                                                                                                    |
| Argp-parse cannot properly filter based on optional TAG as in its documentation (BUG). And leaks when incorrect parameters passed. (realized too late. NOT A SHORTCUT) | Switch to an alternative input handling library                                                                                                                                                                                                                |
| Client takes long to download a package. Freezing the main thread                                                                                                      | Use threading to insert to the DB while the download is taking place to save time.Async/await for the local extraction path. I.e `installed_path`                                                                                                              |

## Tools:

1. IKOS
2. Valgrind
   - Memcheck
   - Callgrind
   - Massif
3. Conan
4. Sqlite3
5. golangci-lint
6. Clangtidy (Custom Style Heavily based of Linux kernel guide)
7. gofmt
8. ppref

# LICENSE:

```
	Show don't Sell License Version 1
https://gitlab.com/SparrowOchon/show-dont-sell/raw/master/LICENSE.md
	Copyright (c) 2019, Network Silence
		All rights reserved.
```
