package main

import (
	"database/sql"
	"errors"
	"fmt"
	
	_ "github.com/mattn/go-sqlite3"
)

type db_item struct {
	Name         string
	Source       string
	Extension    string
	Version      string
	Initial_date string
}

func connect_db() *sql.DB {
	db_conn, db_err := sql.Open("sqlite3", "packages.db")
	if db_conn == nil || db_err != nil {
		fmt.Println(db_err)
		panic("DB file not found packages.db")
	}
	return db_conn
}

func fetch_package(db_conn *sql.DB, package_name string) (db_item, error) {
	var query_result db_item
	sql_query := `SELECT name,source,extension,version,initial_date from packages where name like (?);`

	prepared_sql, query_prep_err := db_conn.Prepare(sql_query)
	error_notification := check_error(query_prep_err,
		"Unable to connect to package store")
	defer prepared_sql.Close()

	if error_notification == nil {
		query_exec_err := prepared_sql.QueryRow(package_name).Scan(
			&query_result.Name, &query_result.Source, &query_result.Extension, &query_result.Version,
			&query_result.Initial_date)
		error_notification = check_error(query_exec_err,
			"Unable to fetch any package named "+package_name)
	}
	return query_result, error_notification
}

func fetch_all_packages(db_conn *sql.DB) ([]db_item, error) {
	var query_result []db_item

	sql_query := `SELECT name, source, extension, version, initial_date from packages;`

	db_item_list, query_prep_err := db_conn.Query(sql_query)
	error_notification := check_error(query_prep_err,
		"Unable to connect to packages store")
	defer db_item_list.Close()
	if error_notification == nil {
		for db_item_list.Next() {
			item := db_item{}
			query_fetch_err := db_item_list.Scan(
				&item.Name, &item.Source, &item.Extension,
				&item.Version, &item.Initial_date)
			error_notification = check_error(query_fetch_err,
				"Unable to fetch any package")
			query_result = append(query_result, item)
		}
	}
	return query_result, error_notification
}
func check_error(err error, error_msg string) error {
	var custom_error error
	custom_error = nil
	if err != nil {
		custom_error = errors.New(error_msg)
	}
	return custom_error
}
