package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

var db_conn = connect_db()

func main() {
	var router = mux.NewRouter()
	router.HandleFunc("/", list_packages).Methods("GET")
	router.HandleFunc("/{package_name}", find_package).Methods("GET")

	fmt.Println("Running server!")
	log.Fatal(http.ListenAndServe(":3000", router))
}

func find_package(w http.ResponseWriter, r *http.Request) {
	received_vars := mux.Vars(r)
	//package_name := received_vars["package_name"]
	query_results, err := fetch_package(db_conn, received_vars["package_name"])
	print_json([]db_item{query_results}, err, w)
}

func list_packages(w http.ResponseWriter, r *http.Request) {
	query_results, err := fetch_all_packages(db_conn)
	print_json(query_results, err, w)
}

func print_json(query_results []db_item, err error, w http.ResponseWriter) {
	w.Header().Add("Content-Type", "application/json")
	if err != nil {
		json.NewEncoder(w).Encode([1]string{err.Error()})
		fmt.Println(err)
	} else {
		json.NewEncoder(w).Encode(query_results)
	}
}
